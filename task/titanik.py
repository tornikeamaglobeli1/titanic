import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    median_values = data.groupby(data['Name'].str.extract(r'(Mr\.|Mrs\.|Miss\.)', expand=False))['Age'] \
                        .agg(['count', 'median']) \
                        .reset_index()
    median_values.columns =["Title","Count","Median"]

    for index, row in median_values.iterrows():
        title = row['Title']
        median_age = round(row['Median'])
        data.loc[(data['Name'].str.contains(title)) & (data['Age'].isnull()), 'Age'] = median_age

    remaining_missing = data['Age'].isnull().sum()
    result = [(row['Title'], int(row['Count']), int(round(row['Median']))) for index, row in median_values.iterrows()]


    return result, remaining_missing
